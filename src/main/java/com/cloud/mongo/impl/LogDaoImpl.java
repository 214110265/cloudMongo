package com.cloud.mongo.impl;

import com.cloud.mongo.dao.LogDao;
import com.cloud.mongo.model.LogSystemModel;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Repository("logDaoImpl")
public class LogDaoImpl implements LogDao {
    @Resource
    private MongoTemplate mongoTemplate;

    public List<LogSystemModel> findAlls() {
        Query query = new Query().with(new Sort(Sort.Direction.DESC, "id"));
        List<LogSystemModel> result = mongoTemplate.find(query, LogSystemModel.class, "logs");
        return result;
    }

    @Override
    public void insert(LogSystemModel object) {
        mongoTemplate.insert(object, "logs");
    }

    @Override
    public long findAllCount() {
        long count = mongoTemplate.count(new Query(), LogSystemModel.class, "logs");
        return count;
    }

    @Override
    public List<LogSystemModel> findAll(int current, int pages) {
        Query query = new Query().skip((current - 1) * pages).limit(pages).with(new Sort(Sort.Direction.DESC, "id"));
        List<LogSystemModel> result = mongoTemplate.find(query, LogSystemModel.class, "logs");
        return result;
    }

    @Override
    public List<LogSystemModel> findDate(String dateTimes, int current, int pages) {
//        LocalDateTime与String日期互相转换
//        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDateTime time = LocalDateTime.now();
//        String localTime = df.format(time);
//        LocalDateTime ldt = LocalDateTime.parse("2017-09-28",df);
//        System.out.println("LocalDateTime转成String类型的时间："+localTime);
//        System.out.println("String类型的时间转成LocalDateTime："+ldt);
//        控制台结果：
//        LocalDateTime转成String类型的时间：2017-09-30 10:40:06
//        String类型的时间转成LocalDateTime：2017-09-28T17:07:05
//        DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/uuuu");
//        LocalDate date = LocalDate.parse("24/06/2014", f);


        String start = dateTimes.substring(0, 10);
        String end = dateTimes.substring(11, 21);
        System.out.println("start:" + start + "   end:" + end);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startTime = LocalDate.parse(start, formatter);
        LocalDate endTime = LocalDate.parse(end, formatter);

        Criteria criteria = Criteria.where("dateTime").gte(startTime).lte(endTime);
        Query query = new Query(criteria).skip((current - 1) * pages).limit(pages).with(new Sort(Sort.Direction.DESC, "id"));

        List<LogSystemModel> result = mongoTemplate.find(query, LogSystemModel.class, "logs");
        return result;
    }

    @Override
    public List<LogSystemModel> findKeyword(String keyword, int current, int pages) {
//        Criteria criteria = Criteria.where("date").regex("");

//        DBObject queryObject = new BasicDBObject();
//        queryObject.put("id",new ObjectId(id));
//        DBObject fields = new BasicDBObject();
//        fields.put("_id",false);
//        fields.put("accountInfos",true);


//        Gson gson = new GsonBuilder()
//                .excludeFieldsWithModifiers(Modifier.PROTECTED) // <—
//                .create();


//        Criteria criteria = Criteria.where("dateTime");
//
//        List<LogSystemModel> result = mongoTemplate.find(query, LogSystemModel.class, "logs");


        return null;
    }
}
