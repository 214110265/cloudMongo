package com.cloud.mongo.model;

public class ResultBody {
    public boolean success;

    public int errorCode;

    public String msg;

    /**
     * 日志
     */
    public LogResultBody logs;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LogResultBody getLogs() {
        return logs;
    }

    public void setLogs(LogResultBody logs) {
        this.logs = logs;
    }
}
