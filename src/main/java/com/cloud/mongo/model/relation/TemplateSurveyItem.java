package com.cloud.mongo.model.relation;
/**

 * 勘察模板分组下的字段

 * @author Pawn

 *

 */
public class TemplateSurveyItem {

    private Integer fiid;//字段id

    private String  fieldName;//字段名

    private Integer fgid;//字段分组的id

    private String groupName;//

    private String fieldUnit;//属性的单位名称

    //属性的二级分类 属性的类型


    public Integer getFiid() {
        return fiid;
    }
    public void setFiid(Integer fiid) {
        this.fiid = fiid;
    }
    public String getFieldName() {
        return fieldName;
    }
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    public Integer getFgid() {
        return fgid;
    }
    public void setFgid(Integer fgid) {
        this.fgid = fgid;
    }
    public String getGroupName() {
        return groupName;
    }
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public String getFieldUnit() {
        return fieldUnit;
    }
    public void setFieldUnit(String fieldUnit) {
        this.fieldUnit = fieldUnit;
    }

}