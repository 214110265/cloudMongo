package com.cloud.mongo.model.relation;

/**
 * 勘察模板的实体
 *
 * @author Pawn
 */
public class TemplateSurvey {

    private Integer tsid;
    private String templateName;//模板名

    private int landType;//土地类型

    private int landUse;//土地用途

    private String caliberName;//口径表名称

    private Integer tcid;//口径表的id

    private int templateStatus;//模板的状态

    private String templateNumber;//模板的编号


    public Integer getTsid() {
        return tsid;
    }

    public void setTsid(Integer tsid) {
        this.tsid = tsid;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public int getLandType() {
        return landType;
    }

    public void setLandType(int landType) {
        this.landType = landType;
    }

    public int getLandUse() {
        return landUse;
    }

    public void setLandUse(int landUse) {
        this.landUse = landUse;
    }

    public String getCaliberName() {
        return caliberName;
    }

    public void setCaliberName(String caliberName) {
        this.caliberName = caliberName;
    }

    public Integer getTcid() {
        return tcid;
    }

    public void setTcid(Integer tcid) {
        this.tcid = tcid;
    }

    public int getTemplateStatus() {
        return templateStatus;
    }

    public void setTemplateStatus(int templateStatus) {
        this.templateStatus = templateStatus;
    }

    public String getTemplateNumber() {
        return templateNumber;
    }

    public void setTemplateNumber(String templateNumber) {
        this.templateNumber = templateNumber;
    }

}