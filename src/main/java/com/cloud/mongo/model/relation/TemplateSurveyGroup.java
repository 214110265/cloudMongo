package com.cloud.mongo.model.relation;

/**
 * 勘察模板的分组实体
 *
 * @author Pawn
 */
public class TemplateSurveyGroup {
    private Integer fgid;//id

    private String groupName;//分组名

    private Integer tsid;//所属勘察模板的id

    private String templateName;//勘察模板的名称

    private int type;//分组的类型


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getFgid() {
        return fgid;
    }

    public void setFgid(Integer fgid) {
        this.fgid = fgid;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getTsid() {
        return tsid;
    }

    public void setTsid(Integer tsid) {
        this.tsid = tsid;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
}