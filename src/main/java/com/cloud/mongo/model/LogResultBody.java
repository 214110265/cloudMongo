package com.cloud.mongo.model;

import java.util.List;

public class LogResultBody {
    /**
     * 当前页
     */
    private int current;
    /**
     * 总页数
     */
    private long totalSize;

    public List<LogSystemModel> logList;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public List<LogSystemModel> getLogList() {
        return logList;
    }

    public void setLogList(List<LogSystemModel> logList) {
        this.logList = logList;
    }
}
