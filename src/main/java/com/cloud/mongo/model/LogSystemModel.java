package com.cloud.mongo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

public class LogSystemModel {
    @Id
    private String id;
    /**
     * 用户id
     */
    private String u_id;
    /**
     * 帐号
     */
    @Excel(name = "帐号")
    private String userName;
    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String name;
    /**
     * 操作时间
     */
    @Excel(name = "操作时间", width = 20)
    private LocalDateTime dateTime;
    /**
     * 操作内容
     */
    @Excel(name = "操作内容", width = 80)
    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
