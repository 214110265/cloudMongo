package com.cloud.mongo.controller;

import cn.afterturn.easypoi.entity.vo.NormalExcelConstants;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.view.PoiBaseView;
import com.cloud.mongo.impl.LogDaoImpl;
import com.cloud.mongo.model.LogResultBody;
import com.cloud.mongo.model.LogSystemModel;
import com.cloud.mongo.model.ResultBody;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/log")
public class LogController {
    @Autowired
    public LogDaoImpl mLogDaoImpl;

    private Gson gson;

    public LogController() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getName().equals("id") || f.getName().equals("u_id");
            }

            @Override
            public boolean shouldSkipClass(Class<?> aClass) {
                return false;
            }
        });
        gson = gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    }

    /**
     * 日志插入
     */
    @GetMapping("/insert")
    @ResponseBody
    public void insertLog(String userName, String name, String Content) {
        LogSystemModel log = new LogSystemModel();
        log.setContent("adsfasdf");
        log.setName("aa");
        log.setUserName("bb");
        LocalDateTime date = LocalDateTime.now();
        log.setDateTime(date);
        mLogDaoImpl.insert(log);
    }

    /**
     * 查询所有
     */
    @GetMapping("/findall")
//    @ResponseBody
    public String findAllLog(int current, int pages) {
        //获取总数
        long count = mLogDaoImpl.findAllCount();
        System.out.println("count:" + count);
        ResultBody body = new ResultBody();
        body.setSuccess(true);

        LogResultBody resultBody = new LogResultBody();
        resultBody.setCurrent(current);
        resultBody.setTotalSize(count);

        if (count > 0) {
            List<LogSystemModel> result = mLogDaoImpl.findAll(current, pages);
            resultBody.setLogList(result);
            body.setMsg("数据总数：" + count);
            for (int i = 0; i < result.size(); i++) {
                System.out.println("00: " + result.get(i).getDateTime());
            }
        } else {
            resultBody.setLogList(null);
            body.setMsg("数据总数为空");
        }
        body.setLogs(resultBody);
        return gson.toJson(body);
    }

    /**
     * 日期查询
     */
    @GetMapping("/finddate")
    @ResponseBody
    public String findLogDate(String dateTimes, int current, int pages) {
        if (dateTimes.length() < 21) {
            ResultBody body = new ResultBody();
            body.setSuccess(true);
            body.setMsg("时间不正确");
            return gson.toJson(body);
        }

        //获取总数
        long count = mLogDaoImpl.findAllCount();
        System.out.println("count:" + count);
        ResultBody body = new ResultBody();
        body.setSuccess(true);
        LogResultBody resultBody = new LogResultBody();
        resultBody.setCurrent(current);
        resultBody.setTotalSize(count);

        if (count > 0) {
            List<LogSystemModel> result = mLogDaoImpl.findDate(dateTimes, current, pages);
            resultBody.setLogList(result);
            final int len = result.size();
            if (len == 0) resultBody.setTotalSize(len);

            System.out.println("count:" + count + " result.size():" + len);
            body.setMsg("数据总数：" + count);
        } else {
            body.setMsg("数据为空");
            resultBody.setLogList(null);
        }
        body.setLogs(resultBody);
        return gson.toJson(body);
    }

    /**
     * 导出日志
     */
    @GetMapping("/exlogs")
    @ResponseBody
    public void exportLogs(ModelMap map, HttpServletRequest request, HttpServletResponse response) {
        List<LogSystemModel> result = mLogDaoImpl.findAlls();

        System.out.println("result.size():" + result.size());

        ExportParams params = new ExportParams("日志列表", "日志 ", ExcelType.XSSF);
        map.put(NormalExcelConstants.DATA_LIST, result);
        map.put(NormalExcelConstants.CLASS, LogSystemModel.class);
        map.put(NormalExcelConstants.PARAMS, params);
        map.put(NormalExcelConstants.FILE_NAME, "日志操作记录" + LocalDate.now());
        PoiBaseView.render(map, request, response, NormalExcelConstants.EASYPOI_EXCEL_VIEW);
    }
}
