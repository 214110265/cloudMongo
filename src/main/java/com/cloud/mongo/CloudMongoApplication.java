package com.cloud.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CloudMongoApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(CloudMongoApplication.class, args);
    }
}
