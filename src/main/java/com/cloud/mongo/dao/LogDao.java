package com.cloud.mongo.dao;

import com.cloud.mongo.model.LogSystemModel;

import java.util.List;

public interface LogDao {
    public void insert(LogSystemModel object);

    public long findAllCount();

    public List<LogSystemModel> findAll(int current, int pages);

    /**
     * 按时间查询
     */
    public List<LogSystemModel> findDate(String dateTimes, int current, int pages);

    /**
     * 按关键字查询
     */
    public List<LogSystemModel> findKeyword(String keyword, int current, int pages);
}
